from trytond.pool import Pool
from . import party
from . import country
from . import rut
from . import configuration
from . import account

__all__ = ['register']


def register():
    Pool.register(
        party.Party,
        party.PartyIdentifier,
        party.PartyTaxLevelCode,
        party.Address,
        country.Subdivision,
        rut.TaxLevelCode,
        configuration.Configuration,
        account.Account,
        account.Move,
        account.Line,
        module='account_co_co', type_='model')
    Pool.register(
        module='account_co_co', type_='wizard')
    Pool.register(
        module='account_co_co', type_='report')
