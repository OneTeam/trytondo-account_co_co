"""
Convierte el archivo departamentos_colombia.csv
en subdivisions_colombia.xml
"""

import csv
model = 'country.subdivision'
subdivisions = open('subdivisions_colombia.csv','r')

subdivisions_xml = open('subdivisions_colombia.xml','w')

subdivisions_reader = csv.reader(subdivisions)
subdivisions_header = next(subdivisions_reader)
d_fields = {subdivisions_header[x]:x for x in range(0,len(subdivisions_header))}

Subdivisions = {}

subdivisions_xml.write('''<?xml version="1.0"?>
<tryton>
    <data>
''')
subdivisions_xml.write('''        <record model="country.country" id="50">
            <field name="name">Colombia</field>
            <field name="code">CO</field>
            <field name="code3">COL</field>
            <field name="code_numeric">170</field>
        </record>
''')


for row in subdivisions_reader:
    dane_code = row[d_fields['DANE']]
    country_id = "50"
    subdivisions_xml.write('''	    <record model="{model}"  id="{id}">
            <field name="name">{name}</field>
            <field name="type">{types}</field>
            <field name="country" ref="50"/>
            <field name="code">{code}</field>
            <field name="dane_code">{dane_code}</field>
        </record>
'''.format(
	model=model,
	id="CO-"+str(dane_code),
	name=row[d_fields['Department']],
        types = row[d_fields['Type']],
        code="CO-"+str(dane_code),
	dane_code=dane_code
	))
subdivisions_xml.write("""    </data>
</tryton>
"""
)
subdivisions.close()
subdivisions_xml.close()
