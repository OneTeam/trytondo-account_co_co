"""
Convierte el archivo departamentos_colombia.csv
en subdivisions_colombia.xml
"""

import csv
model = 'party.tax_level_code'
tipo_responsabilidad = open('tipo_responsabilidad.csv','r')

tipo_responsabilidad_xml = open('tipo_responsabilidad.xml','w')

tipo_responsabilidad_reader = csv.reader(tipo_responsabilidad)
tipo_responsabilidad_header = next(tipo_responsabilidad_reader)
d_fields = {tipo_responsabilidad_header[x]:x for x in range(0,len(tipo_responsabilidad_header))}

TipoResponsabilidad = {}

tipo_responsabilidad_xml.write('''<?xml version="1.0"?>
<tryton>
    <data>
''')



for row in tipo_responsabilidad_reader:
    tipo_responsabilidad_xml.write('''	    <record model="{model}"  id="{id}">
            <field name="name">{name}</field>
            <field name="code">{code}</field>
        </record>
'''.format(
	model=model,
	id=row[d_fields['Código']],
	name=row[d_fields['Significado']],
        code=row[d_fields['Código']]
	))
tipo_responsabilidad_xml.write("""    </data>
</tryton>
"""
)
tipo_responsabilidad.close()
tipo_responsabilidad_xml.close()
