"""
Convierte el archivo municipios_colombia.csv y departamentos_colombia.csv
en municipios_colombia.xml
"""

import csv
model = 'country.subdivision'
municipalities = open('municipalities_colombia.csv','r')
subdivisions = open('subdivisions_colombia.csv','r')

municipalities_xml = open('municipalities_colombia.xml','w')

municipalities_reader = csv.reader(municipalities)
subdivisions_reader = csv.reader(subdivisions)
subdivisions_header = next(subdivisions_reader)
d_fields = {subdivisions_header[x]:x for x in range(0,len(subdivisions_header))}

Subdivisions = {}
for subdivision in subdivisions_reader:
	dane_code = int(subdivision[d_fields['DANE']])
	Subdivisions [dane_code] = dict(zip(list(d_fields.keys()),subdivision))
subdivisions.close()

municipalities_header = next(municipalities_reader)
m_fields = {municipalities_header[x]:x for x in range(0,len(municipalities_header))}



municipalities_xml.write("""<?xml version="1.0"?>
<tryton>
    <data>
""")
for row in municipalities_reader:
	dane_code = row[m_fields['department']] + row[m_fields['code']]
	depto_id = "CO-" + Subdivisions[int(row[m_fields['department']])]['DANE']
	municipalities_xml.write("""		<record model="{model}"  id="{id}">
			<field name="name">{name}</field>
			<field name="dane_code">{dane_code}</field>
        	<field name="country" ref="50"/>
			<field name="type">{type}</field>
			<field name="code">{code}</field>
			<field name="parent" ref="{parent}"/>
		</record>
""".format(
	model=model,
	id="CO-"+str(dane_code),
	name=row[m_fields['name']],
	code="CO-"+str(dane_code),
	dane_code=dane_code,
	depto_id=depto_id,
	type="municipality",
	parent="CO-"+row[m_fields['department']]
	))
municipalities_xml.write("""
    </data>
</tryton>
"""
)
municipalities.close()
municipalities_xml.close()
