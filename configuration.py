from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Configuration']

class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    default_draft_sequence = fields.Many2One(
        'ir.sequence', "Draft Sequence")
