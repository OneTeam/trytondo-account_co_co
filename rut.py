from trytond.model import fields, ModelSQL, ModelView



class TaxLevelCode(ModelSQL, ModelView):
    "Tax Level Code"
    __name__ = 'party.tax_level_code'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
