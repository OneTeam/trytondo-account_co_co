import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class AccountCoCoTestCase(ModuleTestCase):
    'Test Account Co Co module'
    module = 'account_co_co'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            AccountCoCoTestCase))
    return suite
