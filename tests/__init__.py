try:
    from trytond.modules.account_co_co.tests.test_account_co_co import suite  # noqa: E501
except ImportError:
    from .test_account_co_co import suite

__all__ = ['suite']
