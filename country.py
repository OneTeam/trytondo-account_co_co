# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, DeactivableMixin
from trytond.pool import  PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction


__all__ = ['Subdivision']




class Subdivision(ModelSQL, ModelView):
    "Subdivision"

    __metaclass__ = PoolMeta
    __name__ = 'country.subdivision'

    dane_code = fields.Char('Codigo Dane',
                            help="Codigo Dane, usado en Colombia"
                )
